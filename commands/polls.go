package commands

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/ucgc/discord/globalScope"
)

func StartPoll(cxt *globalScope.Context) {
	if !cxt.CheckPermission(discordgo.PermissionManageServer) {
		cxt.PermissionMsgSend("Manage Server")
		return
	}

}
