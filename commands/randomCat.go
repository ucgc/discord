package commands

import (
	"io/ioutil"
	"net/http"

	"encoding/json"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ucgc/discord/globalScope"
)

func GetRandomCat(cxt *globalScope.Context) {
	endPoint := "http://random.cat/meow"
	httpResponse, _ := http.Get(endPoint)

	tmp := struct {
		URL string `json:"file"`
	}{}

	readBody, _ := ioutil.ReadAll(httpResponse.Body)

	json.Unmarshal(readBody, &tmp)

	cxt.Session.ChannelMessageSendEmbed(cxt.Channel.ID, &discordgo.MessageEmbed{
		Title: "Here is your random Cat! Courtesy of: " + cxt.User.Username,
		Color: 0xba9800,
		Image: &discordgo.MessageEmbedImage{
			URL: tmp.URL,
		},
	})
}
