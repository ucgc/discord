# Grab latest golang image from Docker Store
FROM golang:latest

# Set current working directory
WORKDIR /go/src/gitlab.com/ucgc/discord

# Copy contents to WORKDIR
ADD . /go/src/gitlab.com/ucgc/discord

# Install Dependencies
RUN go get -u github.com/golang/dep/cmd/dep
RUN dep ensure

# Build App & Copy config file
RUN go build -o /app/ucgc .
ADD config.json /app

# Run App
CMD [ "/app/ucgc" ]