package main

import (
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/ucgc/discord/dcontext"
	"gitlab.com/ucgc/discord/services"
	"golang.org/x/time/rate"
)

var (
	rateLimits map[string]*rate.Limiter
)

func init() {
	rateLimits = make(map[string]*rate.Limiter)
}

func messageEvent(s *discordgo.Session, message *discordgo.MessageCreate) {
	if message.Author.Bot || !dcontext.MentionCheck(message.Content) {
		return
	}

	context, err := dcontext.BuildMessageContext(s, message)
	if err != nil {
		logrus.Errorf("Could not build Message Context: %v", err)
	}

	cmdDetails, err := dcontext.ReturnCommand(context)
	if err != nil {
		logrus.Errorf("Cannot find Command Document (Did you add it to the DB yet?): %v", err)
		context.Session.ChannelMessageSend(context.Channel.ID, "Unknown Command")
		return
	}

	cmd, found := dcontext.Commands[cmdDetails.Key]
	if !found {
		context.Session.ChannelMessageSend(context.Channel.ID, "Unknown Command")
		return
	}
	if !dcontext.PermissionCheck(context, cmdDetails) {
		context.Session.ChannelMessageSend(context.Channel.ID, "You do not have permissions for this command.")
		return
	}

	if len(cmdDetails.PermittedChannels) > 0 {
		if !permitedChannelsCheck(cmdDetails, context.Channel.ID) {
			context.Session.ChannelMessageSend(context.Channel.ID, "You cannot use that here.")
			return
		}
	}

	if cmdDetails.RateLimited {
		var limiter *rate.Limiter
		var foundL bool

		if limiter, foundL = rateLimits[context.User.Username+context.Command.Root]; !foundL {
			rateLimits[context.User.Username+context.Command.Root] = rate.NewLimiter(rate.Every(1*time.Minute), 1)
		}

		if !limiter.Allow() {
			context.Session.ChannelMessageSend(context.Channel.ID, "You must wait before using this again")
			return
		}

		limiter.Allow()
	}

	cmd(context)
}

func permitedChannelsCheck(cmd *dcontext.CommandDetails, channelID string) bool {
	for _, chanID := range cmd.PermittedChannels {
		if chanID == channelID {
			return true
		}
	}
	return false
}

func presenceEvent(discordSession *discordgo.Session, presenceUpdate *discordgo.PresenceUpdate) {
	guild, err := discordSession.State.Guild(presenceUpdate.GuildID)
	if err != nil {
		logrus.Error(err)
	}

	msgContext := &dcontext.Context{
		Session: discordSession,
		State:   discordSession.State,
		Guild:   guild,
		Channel: nil,
		User:    presenceUpdate.User,
	}

	services.TagLiveStatus(msgContext)
}
