package dcontext

import (
	"github.com/bwmarrin/discordgo"
	"github.com/go-redis/redis"
	"gopkg.in/mgo.v2"
)

var (
	RedisClient   *redis.Client
	MongoClient   *mgo.Session
	BotUser       *discordgo.User
	Commands      map[string]func(*Context)
	DB            = &CommandDB{}
	PermissionMap map[string]int
)
