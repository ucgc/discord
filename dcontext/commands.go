package dcontext

import (
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"

	"gopkg.in/mgo.v2/bson"

	"github.com/Sirupsen/logrus"
)

func init() {

	Commands = map[string]func(*Context){
		"db.add":              AddCommand,
		"db.update.alias":     UpdateAliases,
		"db.update.perms":     UpdatePermissions,
		"db.find":             FindRecords,
		"db.update.ratelimit": UpdateRateLimit,
		"db.delete":           DeleteDocument,
	}
}

// AddCommand adds a command document to mongo
func AddCommand(cxt *Context) {
	mongoError := cxt.Mongo.DB("discord").C("commands").Insert(&CommandDetails{
		Key:         cxt.Command.Args[0],
		Alias:       []string{},
		Permission:  0,
		Description: strings.Join(cxt.Command.Args[1:], " "),
	})

	if mongoError != nil {
		logrus.Errorf("Cannot Insert Document: %v", mongoError)
		return
	}
}

//UpdateAliases updates a document in the command collection
func UpdateAliases(cxt *Context) {
	if len(cxt.Command.Args) < 2 {
		cxt.Session.ChannelMessageSend(cxt.Channel.ID, "Incorrect Parameters need ``@UCGC alias <original> <aliases[]>``")
		return
	}
	selector := bson.M{"commandroot": cxt.Command.Args[0]}
	update := bson.M{"$set": bson.M{"alias": cxt.Command.Args[1:]}}

	mongoError := cxt.Mongo.DB("discord").C("commands").Update(selector, update)

	if mongoError != nil {
		logrus.Errorf("Cannot Update Document Alias: %v", mongoError)
		return
	}
}

func UpdatePermissions(cxt *Context) {
	if len(cxt.Command.Args) < 2 {
		cxt.Session.ChannelMessageSend(cxt.Channel.ID, "Incorrect Parameters need ``@UCGC perms <command> <perms[]>``")
		return
	}
	selector := bson.M{"key": cxt.Command.Args[0]}
	update := bson.M{"$set": bson.M{"permission": cxt.Command.Args[1]}}

	mongoError := cxt.Mongo.DB("discord").C("commands").Update(selector, update)

	if mongoError != nil {
		logrus.Errorf("Cannot Update Document Alias: %v", mongoError)
		return
	}
}

func UpdateRateLimit(cxt *Context) {
	if len(cxt.Command.Args) < 2 {
		cxt.Session.ChannelMessageSend(cxt.Channel.ID, "Incorrect Parameters need ``@UCGC ratelimit <command> <true/false>``")
		return
	}
	selector := bson.M{"key": cxt.Command.Args[0]}
	parsedBool, _ := strconv.ParseBool(cxt.Command.Args[1])
	update := bson.M{"$set": bson.M{"ratelimited": parsedBool}}

	mongoError := cxt.Mongo.DB("discord").C("commands").Update(selector, update)

	if mongoError != nil {
		logrus.Errorf("Cannot Update Document Alias: %v", mongoError)
		return
	}
}

func DeleteDocument(cxt *Context) {
	if len(cxt.Command.Args) < 2 {
		cxt.Session.ChannelMessageSend(cxt.Channel.ID, "Incorrect Parameters need ``@UCGC delete <collection> <commandroot>``")
		return
	}
	selector := bson.M{"key": cxt.Command.Args[1]}

	mongoError := cxt.Mongo.DB("discord").C(cxt.Command.Args[0]).Remove(selector)
	if mongoError != nil {
		logrus.Errorf("Cannot Delete Document: %v", mongoError)
		return
	}
}

func FindRecords(cxt *Context) {
	var result []CommandDetails
	mongoError := cxt.Mongo.DB("discord").C("commands").Find(nil).Iter().All(&result)
	if mongoError != nil {
		logrus.Errorf("Cannot Update Document: %v", mongoError)
		return
	}

	for _, command := range result {
		emf := make([]*discordgo.MessageEmbedField, 0)
		emf = append(emf, &discordgo.MessageEmbedField{
			Name:   "Command: " + command.Key,
			Value:  command.Description + "\u200b",
			Inline: false,
		})
		emf = append(emf, &discordgo.MessageEmbedField{
			Name:   "Aliases",
			Value:  strings.Join(command.Alias, " ") + "\u200b",
			Inline: true,
		})
		emf = append(emf, &discordgo.MessageEmbedField{
			Name:   "Permissions",
			Value:  strconv.Itoa(command.Permission),
			Inline: true,
		})
		emf = append(emf, &discordgo.MessageEmbedField{
			Name:   "Rate Limited",
			Value:  strconv.FormatBool(command.RateLimited) + "\u200b",
			Inline: true,
		})
		emf = append(emf, &discordgo.MessageEmbedField{
			Name:   "Permitted Channels",
			Value:  strings.Join(command.PermittedChannels, " ") + "\u200b",
			Inline: true,
		})
		channel, _ := cxt.Session.UserChannelCreate(cxt.User.ID)
		_, err := cxt.Session.ChannelMessageSendEmbed(channel.ID, &discordgo.MessageEmbed{
			Title:  "Query Results",
			Fields: emf,
		})

		if err != nil {
			logrus.Errorf("%v", err)
		}
	}

}

func ReturnCommand(cxt *Context) (*CommandDetails, error) {
	var result *CommandDetails

	selector := bson.M{
		"$or": []bson.M{
			bson.M{"alias": cxt.Command.Root},
			bson.M{"commandroot": cxt.Command.Root},
		}}

	if mongoError := cxt.Mongo.DB("discord").C("commands").Find(selector).One(&result); mongoError != nil {
		return nil, mongoError
	}

	logrus.Infof("%v", result)
	return result, nil
}
