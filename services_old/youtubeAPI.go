package services_old

import (
	"net/http"
	"strings"

	"gitlab.com/ucgc/discord/globalScope"

	"github.com/Sirupsen/logrus"
	"github.com/google/google-api-go-client/googleapi/transport"
	"github.com/rylio/ytdl"
	"gitlab.com/ucgc/discord/services/music"
	"google.golang.org/api/youtube/v3"
)

var (
	audioID     string
	audioTitle  string
	audioURL    string
	videoURLStr string
)

// QueryYoutube parses the Youtube API and returns the first result
func QueryYoutube(cxt *globalScope.Context) (*music.Song, error) {
	client := &http.Client{
		Transport: &transport.APIKey{Key: cxt.APIs.Youtube},
	}

	service, err := youtube.New(client)
	if err != nil {
		logrus.Error("Error Cannot create Youtube Client: ", err)
		return nil, err
	}

	if !strings.Contains(cxt.Command.Query, "https://www.youtube.com/watch?v=") {
		call := service.Search.List("id,snippet").Q(cxt.Command.Query).MaxResults(1)
		response, err := call.Do()
		if err != nil {
			logrus.Error("Cannot make Youtube API call: ", err)
			return nil, err
		}

		for _, item := range response.Items {
			audioID = item.Id.VideoId
			audioTitle = item.Snippet.Title
		}
		if audioID == "" {
			logrus.Error("Cannot find song")
			return nil, err
		}

		audioURL = "https://www.youtube.com/watch?v=" + audioID
	} else {
		audioURL = cxt.Command.Query
	}

	video, err := ytdl.GetVideoInfo(audioURL)
	if err != nil {
		logrus.Error("Cannot search for Video Info: ", err)
		return nil, err
	}

	format := video.Formats.Extremes(ytdl.FormatAudioBitrateKey, true)[0]
	videoURL, err := video.GetDownloadURL(format)

	if videoURL != nil {
		videoURLStr = videoURL.String()
	} else {
		logrus.Error("Cannot convert URL to string")
	}

	song := &music.Song{
		Channel:  cxt.Channel,
		User:     cxt.User,
		VideoID:  video.ID,
		Title:    audioTitle,
		Duration: video.Duration.String(),
		VideoURL: videoURLStr,
	}

	return song, nil
}
