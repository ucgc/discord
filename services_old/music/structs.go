package music

import (
	"sync"

	"github.com/bwmarrin/discordgo"
	"github.com/jonas747/dca"
	"github.com/oleiade/lane"
)

// Song contains information to control Music
type Song struct {
	Channel  *discordgo.Channel
	User     *discordgo.User
	VideoID  string
	Title    string
	Duration string
	VideoURL string
}

// VoiceInstance provides the overall package o controling a music session
type VoiceInstance struct {
	Voice      *discordgo.VoiceConnection
	Encoder    *dca.EncodeSession
	Stream     *dca.StreamingSession
	Guild      *discordgo.Guild
	Session    *discordgo.Session
	Channel    *discordgo.Channel
	AudioMutex *sync.Mutex
	NowPlaying Song
	Queue      *lane.Queue
	IsSpeaking bool
	IsPaused   bool
	InChannel  bool
}

var (
	// SongChannel provides package level
	SongChannel chan *Song
)
